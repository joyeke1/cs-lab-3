// Copyright Epic Games, Inc. All Rights Reserved.


#include "Lab3GameModeBase.h"
#include "WorldPawn.h"
#include "UObject/ConstructorHelpers.h"

ALab3GameModeBase::ALab3GameModeBase() {
    
    
    // set default pawn class to our Blueprinted character
    //Asumes the BP Pawn is stored in a folfer Content called "Blueprints and that it's named BP_WorldPawn
    static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_WorldPawn"));
    if (PlayerPawnBPClass.Class != nullptr)
    {
        DefaultPawnClass = PlayerPawnBPClass.Class;
    }
    
}
