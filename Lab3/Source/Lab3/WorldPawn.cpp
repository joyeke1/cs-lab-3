// Fill out your copyright notice in the Description page of Project Settings.


#include "WorldPawn.h"

//Pawn Component Includes
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"

//Pawn Movement Includes
#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "CollisionActor.h"
#include "TriggerActor.h"

// Sets default values
AWorldPawn::AWorldPawn()
{
     // Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
    //PrimaryActorTick.bCanEverTick = true;

    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
    MeshComponent->SetupAttachment(RootComponent);
    HitboxComponent = CreateDefaultSubobject<UBoxComponent>(TEXT("Hitbox"));
    HitboxComponent->SetupAttachment(MeshComponent);

    // Create a camera boom
    /*
    Some of these default settings are questionable at best, but if you don't like the camera setup,
    these can be modified in the Blueprint so feel free to experiment to get better looking results
    */
    CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
    CameraBoom->SetupAttachment(MeshComponent);
    CameraBoom->SetUsingAbsoluteRotation(true); // Don't want arm to rotate when character does
    CameraBoom->TargetArmLength = 500.f;
    CameraBoom->SetRelativeRotation(FRotator(-20.f, -180.f, -0.f));
    CameraBoom->bDoCollisionTest = false; // Don't want to pull camera in when it collides with level

    // Create a camera...
    CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
    CameraComponent->SetupAttachment(CameraBoom, USpringArmComponent::SocketName);
    CameraComponent->bUsePawnControlRotation = false; // Camera does not rotate relative to arm

}

// Called when the game starts or when spawned
void AWorldPawn::BeginPlay()
{
    Super::BeginPlay();

    //Add Input Mapping Context since we're bypassing overriding APlayerController
    if (APlayerController* PlayerController = Cast<APlayerController>(Controller))
    {
        if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
        {
            Subsystem->AddMappingContext(DefaultMappingContext, 0);
        }
    }
}

// Called every frame
void AWorldPawn::Tick(float DeltaTime)
{
    Super::Tick(DeltaTime);
}

// Called to bind functionality to input
void AWorldPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
    Super::SetupPlayerInputComponent(PlayerInputComponent);

    // Set up action bindings directly in the Pawn
    if (UEnhancedInputComponent* EnhancedInputComponent = CastChecked<UEnhancedInputComponent>(InputComponent))
    {
        //Setup extra keyboard inputs
        EnhancedInputComponent->BindAction(MoveAction, ETriggerEvent::Triggered, this, &AWorldPawn::MoveEvent);
        EnhancedInputComponent->BindAction(InteractAction, ETriggerEvent::Triggered, this, &AWorldPawn::InteractEvent);
    }

}

void AWorldPawn::MoveEvent(const FInputActionValue& Value) {
    FVector2D MovementVector = Value.Get<FVector2D>();

    if (Controller != nullptr)
    {
        //OG Resident Evil style movement for simplicity
        //It won't get you there fast or well or intuitively but you will get somewhere...
        FVector NewLocation = GetActorLocation();
        NewLocation += GetActorForwardVector() * MovementVector.Y;
        NewLocation += GetActorRightVector() * MovementVector.X;
        SetActorLocation(NewLocation, true);
    }
}

void AWorldPawn::InteractEvent(const FInputActionValue& Value) {
    if(GEngine)
         GEngine->AddOnScreenDebugMessage(-1, 2.0f, FColor::Yellow, TEXT("Interact Triggered"));
    
    TArray<AActor*>OverlappingActors;
    HitboxComponent->GetOverlappingActors(OverlappingActors);
    for (AActor * actor : OverlappingActors) {
        if (actor->IsA(ACollisionActor::StaticClass())){
            actor->Destroy();
        }
        if (actor->IsA(ATriggerActor::StaticClass())){
            ATriggerActor * tactor = Cast<ATriggerActor>(actor);
            tactor->OnTriggerDelegate.Broadcast();
        }
    }
}

