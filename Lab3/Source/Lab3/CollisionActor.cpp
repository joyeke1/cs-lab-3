// Fill out your copyright notice in the Description page of Project Settings.


#include "CollisionActor.h"

#include "Components/StaticMeshComponent.h"
#include "Components/SphereComponent.h"


// Sets default values
ACollisionActor::ACollisionActor()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
    
    MeshComponent = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
    MeshComponent->SetupAttachment(RootComponent);
    HitboxComponent = CreateDefaultSubobject<USphereComponent>(TEXT("Hitbox"));
    HitboxComponent->SetupAttachment(MeshComponent);

}

// Called when the game starts or when spawned
void ACollisionActor::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACollisionActor::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

