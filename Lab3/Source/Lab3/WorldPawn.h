// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "WorldPawn.generated.h"

UCLASS()
class LAB3_API AWorldPawn : public APawn
{
    GENERATED_BODY()

public:
    // Sets default values for this pawn's properties
    AWorldPawn();

protected:
    // Called when the game starts or when spawned
    virtual void BeginPlay() override;

    //Add Blueprint accessible Components to define the Mesh and the Hit Box
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "World Pawn")
    class UBoxComponent* HitboxComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "World Pawn")
    class UStaticMeshComponent* MeshComponent;

    //Add Input mappings directly to the Pawn
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
    class UInputMappingContext* DefaultMappingContext;
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
    class UInputAction* MoveAction;
    UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input, meta=(AllowPrivateAccess = "true"))
    class UInputAction* InteractAction;

public:
    // Called every frame
    virtual void Tick(float DeltaTime) override;

    //Getters for our components
    FORCEINLINE class UBoxComponent* GetHitboxComponent() const { return HitboxComponent; }
    FORCEINLINE class UStaticMeshComponent* GetMeshComponent() const { return MeshComponent; }
    FORCEINLINE class UCameraComponent* GetCameraComponent() const { return CameraComponent; }
    FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }

    // Called to bind functionality to input
    virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

    /*
    Input events will be called from C++ to avoid BP setup, but we could make these BlueprintImplementableEvents
    Note these functions are BlueprintCallable (i.e. can be added to the BP Event Graph)
    but we won't be doing so in this example project :)
    */
    UFUNCTION(BlueprintCallable)
    void MoveEvent(const FInputActionValue& Value);

    UFUNCTION(BlueprintCallable)
    void InteractEvent(const FInputActionValue& Value);

private:
    //Camera stuff can remain private, but we could expose them to BP if we wanted to if they were protected
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class UCameraComponent* CameraComponent;
    UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
    class USpringArmComponent* CameraBoom;

};
